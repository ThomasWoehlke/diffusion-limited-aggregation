diffusion-limited-aggregation
=============================

Fractals and random walk due to Brownian motion cluster

Abstract
--------

Diffusion-limited aggregation (DLA) is the process whereby particles undergoing a random walk due to Brownian motion cluster together to form aggregates of such particles.

This theory, proposed by T.A. Witten Jr. (not to be confused with Edward Witten) and L.M. Sander in 1981,[1] is applicable to aggregation
in any system where diffusion is the primary means of transport in the system. DLA can be observed in many systems such as electrodeposition,
Hele-Shaw flow, mineral deposits, and dielectric breakdown.


Wikipedia
---------

* http://en.wikipedia.org/wiki/Diffusion-limited_aggregation


Maven Wrapper
-------------
* [https://github.com/takari/maven-wrapper](https://github.com/takari/maven-wrapper) 
* [https://www.baeldung.com/maven-wrapper](https://www.baeldung.com/maven-wrapper) 


Run the Desktop Application
---------------------------

```
git clone https://github.com/phasenraum2010/diffusion-limited-aggregation.git
cd diffusion-limited-aggregation
mvnw -Pdefault clean install exec:java
```


Run the Applet Test
-------------------

```
git clone https://github.com/phasenraum2010/diffusion-limited-aggregation.git
cd diffusion-limited-aggregation
mvnw -Pdefault clean install exec:java
```


Screenshot
----------

![Later Screen](./src/site/resources/img/phasenraum_logo.gif)

![Later Screen](./src/site/resources/img/screen1.png)

![Later Screen](./src/site/resources/img/screen1.png)

Blog and Download
-----------------
* http://thomas-woehlke.blogspot.de/2016/01/diffusion-limited-aggregation.html


